var numPantalla = "";

$(".btn-num").on("click", function() {
  colocarNumero($(this).text());
  numPantalla += $(this).text();
});

$(".btn-op").on("click", function() {
  operar($(this).text());
});

$(".btn-punto").on("click", function() {
  punto()
});

$(".btn-borrar").on("click", function() {
  deleteAll();
});

function deleteAll(){
  numPantalla = "";
  $(".pantalla1 p").text("")
  $(".pantalla2 p").text("")
  $(".pantalla1 span").text("")

}

//Funciones
function colocarNumero(num) {
  $(".pantalla2 p").append(num);
}

function punto(){
  if (!$(".pantalla2 p").text().includes(".")) {
    if ($(".pantalla2 p").text() == ""){
      $(".pantalla2 p").append("0");
      numPantalla += "0";
    }
    $(".pantalla2 p").append(".");
    numPantalla += ".";
  }
}

function operar(op) {
  switch (op) {
    case "DEL":
      borrar();
      break;
    case "/":
      dividir();
      break;
    case "x":
      multiplicar();
      break;
    case "-":
      restar();
      break;
    case "+":
      sumar();
      break;
    case "=":
      resultado();
      break;

    default:

  }
}


function borrar() {
  numPantalla = numPantalla.slice(0, -1);
  $(".pantalla2 p").text(numPantalla);
}

function sumar() {
  console.log(numPantalla);
  if ($(".pantalla1 p").text() != "") {
    calculo();
  } else {
    $(".pantalla1 p").text(numPantalla ? numPantalla : 0);
  }
  $(".pantalla1 span").text("+");
  $(".pantalla2 p").text("");
  numPantalla = "";
}

function restar() {
  if ($(".pantalla1 p").text() != "") {
    calculo();
  } else {
    $(".pantalla1 p").text(numPantalla ? numPantalla : 0);
  }
  $(".pantalla1 span").text("-");
  $(".pantalla2 p").text("");
  numPantalla = "";
}

function multiplicar() {
  if ($(".pantalla1 p").text() != "") {
    calculo();
  } else {
    $(".pantalla1 p").text(numPantalla ? numPantalla : 0);
  }
  $(".pantalla1 span").text("x");
  $(".pantalla2 p").text("");
  numPantalla = "";
  if ($(".pantalla1 p").text() == "NaN") {
    deleteAll();
  }

}

function dividir() {
  if ($(".pantalla1 p").text() != "") {
    calculo();
  } else {
    $(".pantalla1 p").text(numPantalla ? numPantalla : 0);
  }
  $(".pantalla1 span").text("/");
  $(".pantalla2 p").text("");
  numPantalla = "";
  if ($(".pantalla1 p").text() == "NaN") {
    deleteAll();
  }
}

function resultado() {
  calculo();
  $(".pantalla2 p").text($(".pantalla1 p").text());
  numPantalla = $(".pantalla1 p").text();
  $(".pantalla1 p").text("");
  $(".pantalla1 span").text("");
}



function calculo() {

  switch ($(".pantalla1 span").text()) {
    case "/":
      $(".pantalla1 p").text(Number($(".pantalla1 p").text()) / Number(numPantalla));
      break;
    case "x":
      $(".pantalla1 p").text(Number($(".pantalla1 p").text()) * Number(numPantalla));
      break;
    case "-":
      $(".pantalla1 p").text(Number($(".pantalla1 p").text()) - Number(numPantalla));
      break;
    case "+":
      $(".pantalla1 p").text(Number($(".pantalla1 p").text()) + Number(numPantalla));
      break;
  }
}
